<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',
    // preloading 'log' component
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    // application components
    'components' => array(
         'JGoogleAPI' => array(
            'class' => 'ext.JGoogleAPI.JGoogleAPI',
            //Default authentication type to be used by the extension
            'defaultAuthenticationType' => 'serviceAPI',
            //Account type Authentication data
            'serviceAPI' => array(
                'clientId' => '695500753569-3gb2n5basp36v4i1hhbhnaseom5ksthh.apps.googleusercontent.com',
                'clientEmail' => '695500753569-6ofjmvbior18uiuqjm7doi7q9896ap30@developer.gserviceaccount.com',
                'publicKey' => 'bdfaf8631b505abee6ef4f052a9d32a8857d73ed',
                'keyFilePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'key.p12',
                'redirectUri' => 'http://localhost/301/wsi-calendar.lo/index.php/site/success',
            ),
            //You can define one of the authentication types or both (for a Service Account or Web Application Account)
            'webappAPI' => array(
                'clientId' => '695500753569-n5eadt5r7mq4s6g74mjiropk8jibd59l.apps.googleusercontent.com',
                'clientEmail' => 'redishen@gmail.com',
                'clientSecret' => 'VJECjXSgt_bATnlFuIgKGbSw',
                'redirectUri' => 'http://localhost/301/wsi-calendar.lo/index.php/site/test2',
//              'javascriptOrigins' => 'YOUR_WEB_APPLICATION_JAVASCRIPT_ORIGINS',
            ),
            'simpleApiKey' => 'AIzaSyAX7TKAtqRNq_6LU__YWwNZG6Gjv1cJQeU',
            //Scopes needed to access the API data defined by authentication type
            'scopes' => array(
                'serviceAPI' => array(
                    'calendar' => array(
                        'https://www.googleapis.com/auth/calendar',
                    ),
                ),
                'webappAPI' => array(
                    'calendar' => array(
                        'https://www.googleapis.com/auth/calendar',
                    ),
                    'drive' => array(
                        'https://www.googleapis.com/auth/drive',
                    ),
                ),
            ),
        ),
        'wsi' => array(
            'class' => 'WsiGrabber'
        ),
        'xcurl' => array(
            'class' => 'ext.curl.XCurl',
            'options' => array(
                'setOptions' => array(
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_MAXREDIRS => 10,
                    CURLINFO_HEADER_OUT => TRUE,
                ),
            )
        ),
        'db' => array(
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/data.db',
        ),
        // uncomment the following to use a MySQL database
        /*
          'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=testdrive',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          ),
         */
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
);
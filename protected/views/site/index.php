<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

/*
 * https://accounts.google.com/o/oauth2/auth?
    scope=email%20profile&
    state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&
   redirect_uri=https%3A%2F%2Foauth2-login-demo.appspot.com%2Fcode&,
   response_type=code&
   client_id=812741506391.apps.googleusercontent.com&
   approval_prompt=force
 */
$params = array(
    'response_type' => 'code',
    'client_id' => '695500753569-3gb2n5basp36v4i1hhbhnaseom5ksthh.apps.googleusercontent.com',
    'redirect_uri' => 'http://localhost/301/wsi-calendar.lo/index.php/site/success',
    'approval_prompt' => 'force',
    'scope' => 'calendar email profile',
    'state' => 'kokoko',
    'access_type' => 'offline'
);
$url = 'https://www.googleapis.com/auth/calendar?'. http_build_query($params);

?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<a href="<?=$url?>">
    +
</a>

<?= CHtml::link('++++', array('test2')); ?>
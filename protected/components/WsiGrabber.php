<?php

/**
 * Description of WsiLoader
 *
 * @author asfto_sheftelevichdm
 */
Yii::import('ext.EGCal.EGCal');
Yii::import('application.vendor.*');
require_once('simpledom/simple_html_dom.php');

//use Application\Vendor\ZendHttpClient\Client;

class WsiGrabber extends CComponent {

    public $authUrl = 'https://www.wsistudents.com/index.jhtml?username={username}&_D%3Ausername=+&password={password}&_D%3Apassword=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginErrorURL=access_denied.jhtml&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginErrorURL=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginSuccessURL=splash.jhtml&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginSuccessURL=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.login=Accept&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.login=+&x=0&y=0&_DARGS=%2Findex.jhtml&_dynSessConf=-8937274378132049996';
    // public $authUrl = 'https://www.wsistudents.com/index.jhtml?loginInput={username}&_D%3AloginInput=+&passwordInput={password}&_D%3ApasswordInput=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.login.x=87&%2Fatg%2Fuserprofiling%2FProfileFormHandler.login.y=9&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.login=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginErrorURL=access_denied.jhtml&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginErrorURL=+&%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginSuccessURL=splash.jhtml&_D%3A%2Fatg%2Fuserprofiling%2FProfileFormHandler.loginSuccessURL=+&_DARGS=%2Findex.jhtml&_dynSessConf=4664006852232009321';
    public $redirUrl = 'http://www.wsistudents.com/switch2supersds.jhtml?optionPage=/inhome/review.jhtml';
    public $dataUrl = 'http://sdszone1.e-wsi.com/inhome/review.jhtml';
    public $userId;
    public $username;
    public $password;
    public $calendarId;

    public function init() {
        
    }

    public function getHTML() {
        $curl = Yii::app()->xcurl;
        $url = str_replace(array('{username}', '{password}'), array($this->username, $this->password), $this->authUrl);

        $curl->run($url);

        preg_match('/^Set-Cookie:\s*([^;]*)/mi', $curl->getData(), $m);
        $curl->options['cookie'] = $m[1];

        $curl->run($this->redirUrl);
        preg_match("/var\sredirectSessionId\s=\s'(\S*)';/mi", $curl->getData(), $rr);
        $sessionId = $rr[1];

        $curl->options['cookie'] = "JSESSIONID={$sessionId}";
        $curl->options['setOptions'][CURLOPT_ENCODING] = "gzip";

        $curl->run($this->dataUrl);

        if (!$curl->hasErrors()) {
            return $curl->getData();
        } else {
            Yii::log($curl->getErrors()->message, CLogger::LEVEL_ERROR, 'wsi.grabber.curl');
        }
    }

    public function parseTable() {
        $html = $this->getHTML();
//        $html = iconv('ISO-8859-1', 'utf-8', $html);
        $dom = new simple_html_dom();
        $dom->load($html);
        $trs = $dom->find("table tr[ID]");

        $result = array();
        foreach ($trs as $tr) {
            $tds = $tr->find("td");
            $dateElement = $tr->find('input[name^="DAY"]');
            $plainDate = $dateElement[0]->value;

            $timeElement = $tr->find('input[name^="TIME"]');
            $plainTime = $timeElement[0]->value;
            list($timeFrom, $timeTo) = explode('-', trim($plainTime));


            $result[$tr->id] = array(
                'type' => trim($tds[1]->plaintext),
                'timeFrom' => strtotime($plainDate . ' ' . $timeFrom),
                'timeTo' => strtotime($plainDate . ' ' . $timeTo),
                'level' => preg_replace('/\s+/', ' ', $tds[3]->plaintext),
                'description' => html_entity_decode(trim($tds[4]->plaintext)),
            );
        }
        return $result;
    }

    public function makeEvent($param) {
        $api = Yii::app()->JGoogleAPI;
        $service = $api->getService('Calendar');

        $event = $api->getObject('Event', 'Calendar');
//        print_r(get_class_methods($event));
        $event->setSummary($param['type']);
        $event->setDescription($param['level'] . "\n" . $param['description']);
        $event->setLocation('Земляной Вал, 75 (WSI Таганская)');

        $start = $api->getObject('EventDateTime', 'Calendar');
        $start->setDateTime(date('c', $param['timeFrom']));
        $event->setStart($start);
        $end = $api->getObject('EventDateTime', 'Calendar');
        $end->setDateTime(date('c', $param['timeTo']));
        $event->setEnd($end);
//        $event->setColor(1);

        $createdEvent = $service->events->insert($this->calendarId, $event);

        return $createdEvent->getId();
    }

    public function run() {
        $service = Yii::app()->JGoogleAPI->getService('Calendar');

        $api = Yii::app()->JGoogleAPI;
//
//        $color = $api->getObject('Colors', 'Calendar');
//        print_r(get_class_methods($color->getCalendar()));
//
//        return;

        $events = $this->parseTable();
        print "EVENTS ^" . count($events) . "\n";
        if (empty($events)) {
            Yii::log('No events found', CLogger::LEVEL_WARNING, 'wsi.grabber.run');
            print "No events found \n";
            return FALSE;
        }
        foreach ($events as $id => $event) {
            $isCreated = Event::model()->countByAttributes(array(
                'id' => $id,
                'user_id' => $this->userId
            ));

            if ($isCreated == 0) {
                $newId = $this->makeEvent($event);
                $newEvent = new Event();
                $newEvent->id = $id;
                $newEvent->google_id = $newId;
                $newEvent->user_id = $this->userId;
                if ($newEvent->save()) {
                    print "CREATED \n";
                    print "WSI ID ^" . $newEvent->id . "\n";
                    print "GOOGLE ID ^" . $newEvent->google_id . "\n";
                    print "*** \n";
                }
            }
        }
    }

}

?>

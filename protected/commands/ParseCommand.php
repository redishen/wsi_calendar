<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ParseCommand
 *
 * @author asfto_sheftelevichdm
 */
class ParseCommand extends CConsoleCommand {

    public function run($args) {
        $models = User::model()->active()->findAll();

        foreach ($models as $model) {
            $wsi = Yii::app()->wsi;
            $wsi->userId = $model->id;
            $wsi->username = $model->username;
            $wsi->password = $model->password;
            $wsi->calendarId = $model->calendar_id;
            $wsi->run();
        }
    }

}

?>
